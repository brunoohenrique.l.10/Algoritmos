import java.util.Scanner;

public class CalculoMedia {
	
	public static void main(String[] args) {
		double nota_ap1;
		double nota_ap2;
		double nota_ap3;
		double media;
		Scanner s = new Scanner(System.in);
		
		System.out.println("Digite a nota da AP1");
		nota_ap1 = s.nextDouble();
		System.out.println("Digite a nota da AP2");
		nota_ap2 = s.nextDouble();
		System.out.println("Digite a nota da AP3");
		nota_ap3 = s.nextDouble();
		
		media = (nota_ap1 * 0.3) + (nota_ap2 * 0.3) + (nota_ap3 * 0.4);
		
		System.out.println("Sua média final foi " + media);
	
	}

}

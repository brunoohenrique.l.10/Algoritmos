import java.util.Scanner;

public class ParOuImpar {
	
	public static void main(String[] args) {
		int numero;
		Scanner s = new Scanner(System.in);
		
		System.out.println("Digite um número");
		numero = s.nextInt();
		
		if(numero % 2 == 0){
			System.out.println("O número é par");
		}else {
			System.out.println("O número é ímpar");
		}
	}

}

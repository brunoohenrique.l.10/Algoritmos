import java.util.Scanner;

public class Media {
	
	public static void main(String[] args) {
		double nota1;
		double nota2;
		double media;
		
		Scanner s = new Scanner(System.in);
		
		System.out.println("Digite a nota 1");
		nota1 = s.nextDouble();
		
		System.out.println("Digite a nota 2");
		nota2 = s.nextDouble();
		
		media = (nota1 + nota2) / 2;
		
		if (media >= 7) {
			System.out.println("Aprovado");
		} else if(media >= 3){
			double notaFinal;
			System.out.println("Digite a nota da final");
			notaFinal = s.nextDouble();
			
			double mediaFinal = (media + notaFinal)/2;
			
			if (mediaFinal >= 5 ) {
				System.out.println("Aprovdo na final");
			} else {
				System.out.println("Reprovado na final");
			}
			
		}else {
			System.out.println("Reprovado");
		}
	}

}

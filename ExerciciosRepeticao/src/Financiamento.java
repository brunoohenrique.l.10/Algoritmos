import java.util.Scanner;

public class Financiamento {
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		double valorFinanciamento;
		double taxaDeJuros;
		int qtdMeses;
		
		System.out.println("Digite o valor do financiamento");
		valorFinanciamento = scanner.nextDouble();
		System.out.println("Digite a taxa de juros");
        taxaDeJuros = scanner.nextDouble();
        System.out.println("Digite a quantidade de meses do financiamento");
        qtdMeses = scanner.nextInt();
		
		 double saldoDevedorFinal = 0;
         double valorParcelas = 0;

         saldoDevedorFinal = valorFinanciamento;

         for (int i = 0; i < qtdMeses; i++) {
                 saldoDevedorFinal = saldoDevedorFinal + 
                		 (saldoDevedorFinal * taxaDeJuros / 100);
                // System.out.println(saldoDevedorFinal);
         }

         valorParcelas = (saldoDevedorFinal / qtdMeses);
         
         System.out.println("Valor final = R$" + saldoDevedorFinal + 
        		 "\nValor da parcela = R$" + valorParcelas);

	}

}

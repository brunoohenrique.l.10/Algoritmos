import java.util.Scanner;

public class Equacao {
	
	public static void main(String[] args) {
		// res = (nˆn * n!) / n
		int n;
		Scanner s = new Scanner(System.in);
		System.out.println("Digite o valor de n");
		n = s.nextInt();
		
		//calculo do fatorial
		int fat = 1;
		int cont = n;
		while (cont > 1){
			fat *= cont;
			cont--;
		}
		System.out.println ("fat = " + fat);
		
		//calculo da potencia
		
		int base = n;
		cont = 1;
		while (cont < n) {
			base = base * n;
			cont++;
		}
		System.out.println ("pot = " + base);
		
		int res = (base * fat)/n;
		System.out.println("Resultado = " + res);
	}

}

import java.util.Scanner;

public class PrimeiroPrograma {

	public static void main(String[] args) {
		int qtd_anos;
		int qtd_cigarros_dia;
		int preco_carteira;
		Scanner s = new Scanner(System.in);
		
		System.out.println("Digite a quantidade de anos que vc fuma");
		qtd_anos = s.nextInt();
		System.out.println("Digite a quantidade de cigarros que vc fuma por dia");
		qtd_cigarros_dia = s.nextInt();
		System.out.println("Digite o preço da carteira");
		preco_carteira = s.nextInt();
		
		int gasto_total = (qtd_anos * 365 * 
				qtd_cigarros_dia * preco_carteira)/20;
		
		System.out.println("Voce gastou R$ " + gasto_total);
		
	}
}

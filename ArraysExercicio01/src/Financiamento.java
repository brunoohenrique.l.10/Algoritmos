import java.util.Scanner;

public class Financiamento {
	
	public static void main(String[] args) {
		double valorFinanciamento;
		int taxaDeJuros;
		int qtdMeses;
		double saldoDevedorFinal;
		double valorParcelas;
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Digite o valor do financiamento");
		valorFinanciamento = scanner.nextDouble();
		System.out.println("Digite a taxa de juros");
		taxaDeJuros = scanner.nextInt();
		System.out.println("Digite a quantidade de meses do financiamento");
		qtdMeses = scanner.nextInt();
		
		saldoDevedorFinal = valorFinanciamento;
		
		//Calculo do saldo devedor final. Repetimos a quantidade de meses que devemos aplicar os juros mês a mês
		for (int i = 0; i < qtdMeses; i++) {
			saldoDevedorFinal = saldoDevedorFinal + (saldoDevedorFinal * 
					taxaDeJuros/100);
			//System.out.println(saldoDevedorFinal);
		}
		
		valorParcelas = saldoDevedorFinal / qtdMeses;
		
		System.out.println("o seu financiamento de R$ " + valorFinanciamento + " em " + qtdMeses + " meses, terá uma parcelade R$ " 
		+ valorParcelas + ". \nO saldo devedor final foi de R$ " + saldoDevedorFinal + 
		". \nVoce está pagando R$ " + (saldoDevedorFinal - valorFinanciamento) + " de juros.");
		
	}

}

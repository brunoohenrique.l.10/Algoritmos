import java.util.Scanner;

public class Exercicio01 {
	
	public static void main(String[] args) {
		
		int [] valores = new int[5];
		Scanner scanner = new Scanner(System.in);
		
		int maiorPar = 0;
		int menorImpar = 0;
		int somatorio = 0;
		int media;
		boolean achouPar = false;
		boolean achouImpar = false;	
		
		//Preenchendo os valores do array
		for (int i = 0; i < valores.length; i++) {
			System.out.println("Digite o valor " + i + " do array");
			valores[i] = scanner.nextInt();
			if (!achouPar && valores[i] % 2 == 0) {
				 maiorPar = valores[i];
				 achouPar = true;
			} else if (!achouImpar && valores[i] % 2 != 0){
				menorImpar = valores[i];
				achouImpar = true;
			}
		}
		
		for (int i = 0; i < valores.length; i++) {
			if (valores[i] % 2 == 0 && valores[i] > maiorPar) {
				maiorPar = valores[i];
			} else if (valores[i] % 2 != 0 && valores[i] < menorImpar){
				menorImpar = valores[i];
			}
			
			somatorio = somatorio + valores[i];
		}
		
		media = somatorio / valores.length;
		
		if (achouPar){ 
			System.out.println("Maior par = " + maiorPar);
		} else {
			System.out.println("Nenhum valor par encontrado");
		}
		
		if (achouImpar){
			System.out.println("Menor impar = " + menorImpar);
		}else {
			System.out.println("Nenhum valor impar encontrado");
		}
		
		System.out.println("Somatório = " + somatorio);
		System.out.println("Média = " + media);
	}

}

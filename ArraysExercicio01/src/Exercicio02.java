import java.util.Random;
import java.util.Scanner;

public class Exercicio02 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int matrizA[][] = new int[2][2];
		int matrizB[][] = new int[2][2];

		int matrizSoma[][] = new int[2][2];

		// preencher a matriz a
		for (int i = 0; i < matrizA.length; i++) {
			for (int j = 0; j < matrizA.length; j++) {
				System.out.println("Digite o valor [" + i + "," + j + "]");
				matrizA[i][j] = scanner.nextInt();
			}
		}

		// preencher a matriz B
		for (int i = 0; i < matrizB.length; i++) {
			for (int j = 0; j < matrizB.length; j++) {
				System.out.println("Digite o valor [" + i + "," + j + "]");
				matrizB[i][j] = scanner.nextInt();
			}
		}
		
		//Soma das matrizes
		for (int i = 0; i < matrizSoma.length; i++) {
			for (int j = 0; j < matrizSoma.length; j++) {
				matrizSoma[i][j] = matrizA[i][j] + matrizB[i][j];
			}
		}
		
		//Imprime a matriz soma
		for (int i = 0; i < matrizSoma.length; i++) {
			for (int j = 0; j < matrizSoma.length; j++) {
				System.out.println("[" + i + "," + j + "]" + " = " + matrizSoma[i][j]);
			}
		}
		
	}

}

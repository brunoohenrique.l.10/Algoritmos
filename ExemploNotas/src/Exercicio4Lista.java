import java.util.Scanner;

public class Exercicio4Lista {
	public static void main(String[] args) {
		
		double valor1;
		double valor2;
		double valor3;
		
		Scanner s = new Scanner(System.in);
		
		System.out.println("Digite o valor 1");
		valor1 = s.nextDouble();
		
		System.out.println("Digite o valor 2");
		valor2 = s.nextDouble();
		
		System.out.println("Digite o valor 3");
		valor3 = s.nextDouble();
		
		if (valor1 > valor2 + valor3) {
			System.out.println("Valor 1 é maior que a soma de valor 2 e valor 3");
		}else {
			System.out.println("Valor 1 não é maior que a soma de valor 2 e valor 3");
		}
		
	}

}

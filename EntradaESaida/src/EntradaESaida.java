import java.util.Scanner;

public class EntradaESaida {
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		//saída de dados
		System.out.println("Digite um número");
		//entrada de dados 
		int numero = scanner.nextInt();
		
		System.out.println("Digite um nome");
		String nome = scanner.next();
		
		System.out.println("Digite um valor real");
		double saldo = scanner.nextDouble();
		
		//Exemplo de concatenação
		System.out.println("O cliente " + nome + " de conta numero "+ numero + " tem R$ " + saldo + " em conta.");
		
	}

}

import java.util.Scanner;

public class ExerciciosFuncoeseArrays {
	
	static Scanner s = new Scanner(System.in);
	
	static int maiorPar(int [] valores){
		int maiorPar = 1;
		
		for (int i = 0; i < valores.length; i++) {
			if (valores[i] % 2 == 0 && valores[i] > maiorPar) {
				maiorPar = valores[i];
			}
		}
		
		return maiorPar;
	}
	
	static void preencheArray(int [] array){
		for (int i = 0; i < array.length; i++) {
			System.out.println("Digite o elemento " + i + " do array");
			array[i] = s.nextInt();
		}
	}
	

	static int[] intersec (int[] conjuntoA, int[] conjuntoB){
		int cont = 0;
		int result[];
		
		for (int i = 0; i < conjuntoA.length; i++) {
			for (int j = 0; j < conjuntoB.length; j++) {
				if (conjuntoA[i] == conjuntoB[j]){
					cont++;
				}
			}
		}
		
		if (cont == 0) {
			return null;
		} else {
			result = new int[cont];
			int position = 0;
			for (int i = 0; i < conjuntoA.length; i++) {
				for (int j = 0; j < conjuntoB.length; j++) {
					if (conjuntoA[i] == conjuntoB[j]){
						result[position] = conjuntoA[i];
						position++;
					}
				}
			}
			
			return result;
		}
	}
	
	public static void main(String[] args) {
		
		int [] valores = new int[4];
		int [] valores2 = new int[4];
		
		preencheArray(valores);
		preencheArray(valores2);
		
//	    int maiorPar = maiorPar(valores);
//	    
//	    if (maiorPar == 1) {
//	    		System.out.println("Não existe valor par no array");
//	    } else {
//	    		System.out.println("Maior par é " + maiorPar);
//	    }
		
		int[] intersec = intersec(valores, valores2);
		
		
		
		if (intersec != null) {
			System.out.println("Elementos da interseção");
			for (int i = 0; i < intersec.length; i++) {
				System.out.println(intersec[i]);
			}
		} else {
			System.out.println("Não existem elementos na interseção");
		}
		
		
	}

}

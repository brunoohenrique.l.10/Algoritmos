import java.util.Scanner;

public class Funcoes {
	
	static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		int opcao;

		do {

			System.out.println(
					"\n\n\n1 - Calcular Fatorial \n2 - Calculo das parcelas\n"
					+ "3 - Soma de 2 matrizes\n4 - Potencia \n5 - Fibonacci \n0 - Sair \n Digite uma opção");

			opcao = scanner.nextInt();

			switch (opcao) {
			case 1:
				System.out.println("Digite um valor para calcular o fatorial");
				int valor = scanner.nextInt();
				System.out.println("O fatorial de " + valor + " é " + fatorial(valor));
				break;
			case 2:
				double valorFinanciamento;
				double taxaDeJuros;
				int qtdMeses;

				System.out.println("Digite o valor do financiamento");
				valorFinanciamento = scanner.nextDouble();
				System.out.println("Digite a taxa de juros");
				taxaDeJuros = scanner.nextDouble();
				System.out.println("Digite a quantidade de meses do financiamento");
				qtdMeses = scanner.nextInt();

				financiamento(valorFinanciamento, qtdMeses, taxaDeJuros);
				break;
			case 3:
				int[][] matrizSoma = calculoMatrizes();
				for (int i = 0; i < matrizSoma.length; i++) {
					//Usamos matrizSoma[0].lenght para calcularmos quantas colunas tem uma linha.
					for (int j = 0; j < matrizSoma[0].length; j++) {
						System.out.println("[" + i + "," + j + "] = " + matrizSoma[i][j]);
					}
				}
				break;
			case 4: 
				System.out.println("Digite o valor da base");
				int base = scanner.nextInt();
				System.out.println("Digite o valor do expoente");
				int exp = scanner.nextInt();
				System.out.println("A potencia é " + potencia(base, exp));
				break;
			}

		} while (opcao != 0);
	}

	static double fatorial(int numero) {
		double fat = 1;
		for (int i = numero; i > 1; i--) {
			fat = fat * i;
		}
		return fat;
	}

	static void financiamento(double valorFinanciamento, int qtdMeses, double taxaDeJuros) {
		double saldoDevedorFinal = 0;
		double valorParcelas = 0;

		saldoDevedorFinal = valorFinanciamento;

		for (int i = 0; i < qtdMeses; i++) {
			saldoDevedorFinal = saldoDevedorFinal + (saldoDevedorFinal * taxaDeJuros / 100);
		}

		valorParcelas = saldoDevedorFinal / qtdMeses;

		System.out.println("Valor final = R$" + saldoDevedorFinal + "\nValor da parcela = R$" + valorParcelas);
	}

	static int[][] calculoMatrizes() {
		
		int linhas, colunas;
		System.out.println("Digite a quantidade de linhas da matriz");
		linhas = scanner.nextInt();
		System.out.println("Digite a quantidade de colunas da matriz");
		colunas = scanner.nextInt();
		
		int[][] matriz1 = preencheMatriz(linhas, colunas);
		int[][] matriz2 = preencheMatriz(linhas, colunas);
		
		int[][] resultado = new int[linhas][colunas];

		for (int i = 0; i < linhas; i++) {
			for (int j = 0; j < colunas; j++) {
				resultado[i][j] = matriz1[i][j] + matriz2[i][j];
			}
		}
		
		return resultado;
	}
	
	static int[][] preencheMatriz(int linhas, int colunas) {
		int[][]matriz = new int[linhas][colunas];
		for (int i = 0; i < linhas; i++) {
			for (int j = 0; j < colunas; j++) {
				System.out.println("Digite a posição " + i + " , " + j + ":");
				matriz[i][j] = scanner.nextInt();
			}
		}
		
		return matriz;
	}
	
	static double potencia(int valor1, int valor2){
		if (valor2 == 0){
			return 1;
		} else if (valor2 == 1) {
			return valor1;
		} else {
			int result = valor1;
			for (int i = 1; i < valor2; i++) {
				result *= valor1;
			}
			return result;
		}
		
	}
	
	static void fibonacci(int intervalo){
		int valorAnterior = 0;
		int valorAtual = 1;
		int proxValor = 0;
		int valor = 1;
		
		System.out.print(valor);
		
		for (int i = 1; i <= intervalo; i++) {
			proxValor = valorAnterior + valorAtual;
			System.out.print("," + proxValor);
			valorAnterior = valorAtual;
			valorAtual = proxValor;
		}
	}

}

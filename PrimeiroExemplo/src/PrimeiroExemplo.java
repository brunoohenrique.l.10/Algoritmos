import java.util.Scanner;

public class PrimeiroExemplo {
	
	public static void main(String[] args) {
	     int valor;

	     System.out.println("Digite um inteiro");
	     Scanner s = new Scanner(System.in);
	     valor = s.nextInt();  
	     
	     System.out.println("Você digitou " + valor);
	}

}
